#pragma once
/*
 *  Copyright (C) 2013 Alex Deryskyba (alex@codesnake.com)
 *  https://bitbucket.org/codesnake/pvr.sovok.tv_xbmc_addon
 *
 *  This file is part of pvr.sovok.tv XBMC Addon.
 *
 *  Foobar is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with pvr.sovok.tv XBMC Addon. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <exception>

class InputBufferException : public std::exception
{
};

class InputBuffer
{
public:
    virtual ~InputBuffer() {}

    virtual long long GetLength() const = 0;
    virtual long long GetPosition() const = 0;
    virtual int Read(unsigned char *buffer, unsigned int bufferSize) = 0;
    virtual long long Seek(long long iPosition, int iWhence) const = 0;
    virtual bool SwitchStream(const std::string &newUrl) = 0;
};
