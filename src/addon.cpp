/*
 *  Copyright (C) 2013 Alex Deryskyba (alex@codesnake.com)
 *  https://bitbucket.org/codesnake/pvr.sovok.tv_xbmc_addon
 *
 *  This file is part of pvr.sovok.tv XBMC Addon.
 *
 *  Foobar is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with pvr.sovok.tv XBMC Addon. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <stdio.h>
#include <xbmc_addon_dll.h>
#include <xbmc_pvr_dll.h>
#include <libXBMC_pvr.h>
#include <libXBMC_gui.h>
#include "config.h"
#include "sovok_pvr_client.h"

using namespace ADDON;
using namespace std;

CHelper_libXBMC_addon *g_xbmc;
CHelper_libXBMC_pvr *g_pvr;
SovokPVRClient *g_client = NULL;

ADDON_STATUS ADDON_Create(void *callbacks, void* props)
{
    g_xbmc = new CHelper_libXBMC_addon();
    if (!g_xbmc->RegisterMe(callbacks))
    {
        delete g_xbmc;
        return ADDON_STATUS_PERMANENT_FAILURE;
    }

    g_pvr = new CHelper_libXBMC_pvr();
    if (!g_pvr->RegisterMe(callbacks))
    {
        delete g_pvr;
        delete g_xbmc;
        return ADDON_STATUS_PERMANENT_FAILURE;
    }

    string login;
    string password;
    bool isTimeshiftEnabled;
    bool shouldAddFavoritesGroup;
    char buffer[1024];

    if (g_xbmc->GetSetting("login", &buffer))
        login = buffer;
    if (g_xbmc->GetSetting("password", &buffer))
        password = buffer;
    g_xbmc->GetSetting("enable_timeshift", &isTimeshiftEnabled);
    g_xbmc->GetSetting("add_favorites_group", &shouldAddFavoritesGroup);

    try
    {
        g_client = new SovokPVRClient(g_xbmc, g_pvr, login, password);
        g_client->SetTimeshiftEnabled(isTimeshiftEnabled);
        g_client->SetAddFavoritesGroup(shouldAddFavoritesGroup);
    }
    catch (AuthFailedException &)
    {
        g_xbmc->QueueNotification(QUEUE_ERROR, "Login to Sovok.TV failed.");

        delete g_pvr;
        delete g_xbmc;
        return ADDON_STATUS_PERMANENT_FAILURE;
    }

    return ADDON_STATUS_OK;
}

ADDON_STATUS ADDON_GetStatus()
{
    return ADDON_STATUS_OK;
}

void ADDON_Destroy()
{
    delete g_client;
    delete g_xbmc;
    delete g_pvr;
}

bool ADDON_HasSettings()
{
    return false;
}

unsigned int ADDON_GetSettings(ADDON_StructSetting ***sSet)
{
    return 0;
}

ADDON_STATUS ADDON_SetSetting(const char *settingName, const void *settingValue)
{
    if (strcmp(settingName, "enable_timeshift") == 0)
        g_client->SetTimeshiftEnabled(*(bool *)(settingValue));

    if (strcmp(settingName, "add_favorites_group") == 0)
        g_client->SetAddFavoritesGroup(*(bool *)(settingValue));

    if (strcmp(settingName, "streamer") == 0)
    {
        int streamerId = *(int *)(settingValue);
        streamerId++;
        g_client->SetStreamerId(streamerId);
    }

    return ADDON_STATUS_OK;
}

void ADDON_Stop()
{
}

void ADDON_FreeSettings()
{
}

void ADDON_Announce(const char *flag, const char *sender, const char *message, const void *data)
{
}

const char *GetPVRAPIVersion()
{
    return XBMC_PVR_API_VERSION;
}

const char* GetMininumPVRAPIVersion()
{
    return XBMC_PVR_MIN_API_VERSION;
}

const char* GetGUIAPIVersion()
{
    return XBMC_GUI_API_VERSION;
}

const char* GetMininumGUIAPIVersion()
{
    return XBMC_GUI_MIN_API_VERSION;
}

PVR_ERROR GetAddonCapabilities(PVR_ADDON_CAPABILITIES *pCapabilities)
{
    pCapabilities->bSupportsEPG = true;
    pCapabilities->bSupportsTV = true;
    pCapabilities->bSupportsRadio = true;
    pCapabilities->bSupportsRecordings = false;
    pCapabilities->bSupportsTimers = false;
    pCapabilities->bSupportsChannelGroups = true;
    pCapabilities->bSupportsChannelScan = false;
    pCapabilities->bHandlesInputStream = true;
    pCapabilities->bHandlesDemuxing = false;
    pCapabilities->bSupportsRecordingFolders = false;
    pCapabilities->bSupportsRecordingPlayCount = false;
    pCapabilities->bSupportsLastPlayedPosition = false;

    return PVR_ERROR_NO_ERROR;
}

const char* GetBackendName()
{
    return "Sovok.TV";
}

const char* GetBackendVersion()
{
    return PROJECT_VERSION;
}

PVR_ERROR GetEPGForChannel(ADDON_HANDLE handle, const PVR_CHANNEL& channel, time_t iStart, time_t iEnd)
{
    return g_client->GetEPGForChannel(handle, channel, iStart, iEnd);
}

int GetChannelGroupsAmount()
{
    return g_client->GetChannelGroupsAmount();
}

PVR_ERROR GetChannelGroups(ADDON_HANDLE handle, bool bRadio)
{
    return g_client->GetChannelGroups(handle, bRadio);
}

PVR_ERROR GetChannelGroupMembers(ADDON_HANDLE handle, const PVR_CHANNEL_GROUP& group)
{
    return g_client->GetChannelGroupMembers(handle, group);
}

int GetChannelsAmount()
{
    return g_client->GetChannelsAmount();
}

PVR_ERROR GetChannels(ADDON_HANDLE handle, bool bRadio)
{
    return g_client->GetChannels(handle, bRadio);
}

bool OpenLiveStream(const PVR_CHANNEL& channel)
{
    return g_client->OpenLiveStream(channel);
}

void CloseLiveStream()
{
    g_client->CloseLiveStream();
}

int ReadLiveStream(unsigned char* pBuffer, unsigned int iBufferSize)
{
    return g_client->ReadLiveStream(pBuffer, iBufferSize);
}

bool SwitchChannel(const PVR_CHANNEL& channel)
{
    return g_client->SwitchChannel(channel);
}

bool CanPauseStream()
{
    return g_client->IsTimeshiftEnabled();
}

bool CanSeekStream()
{
    return g_client->IsTimeshiftEnabled();
}

long long SeekLiveStream(long long iPosition, int iWhence)
{
    return g_client->SeekLiveStream(iPosition, iWhence);
}

long long PositionLiveStream()
{
    return g_client->PositionLiveStream();
}

long long LengthLiveStream()
{
    return g_client->LengthLiveStream();
}

/******************************************************************************
 *  Unused API functions
 ******************************************************************************/

const char* GetConnectionString()
{
    return "connected";
}

const char* GetLiveStreamURL(const PVR_CHANNEL& channel)
{
    return NULL;
}

PVR_ERROR GetDriveSpace(long long* iTotal, long long* iUsed)
{
    return PVR_ERROR_NOT_IMPLEMENTED;
}

PVR_ERROR CallMenuHook(const PVR_MENUHOOK& menuhook, const PVR_MENUHOOK_DATA &item)
{
    return PVR_ERROR_NOT_IMPLEMENTED;
}

PVR_ERROR DialogChannelScan()
{
    return PVR_ERROR_NOT_IMPLEMENTED;
}

PVR_ERROR DeleteChannel(const PVR_CHANNEL& channel)
{
    return PVR_ERROR_NOT_IMPLEMENTED;
}

PVR_ERROR RenameChannel(const PVR_CHANNEL& channel)
{
    return PVR_ERROR_NOT_IMPLEMENTED;
}

PVR_ERROR MoveChannel(const PVR_CHANNEL& channel)
{
    return PVR_ERROR_NOT_IMPLEMENTED;
}

PVR_ERROR DialogChannelSettings(const PVR_CHANNEL& channel)
{
    return PVR_ERROR_NOT_IMPLEMENTED;
}

PVR_ERROR DialogAddChannel(const PVR_CHANNEL& channel)
{
    return PVR_ERROR_NOT_IMPLEMENTED;
}

int GetRecordingsAmount()
{
    return PVR_ERROR_NOT_IMPLEMENTED;
}

PVR_ERROR GetRecordings(ADDON_HANDLE handle)
{
    return PVR_ERROR_NOT_IMPLEMENTED;
}

PVR_ERROR DeleteRecording(const PVR_RECORDING& recording)
{
    return PVR_ERROR_NOT_IMPLEMENTED;
}

PVR_ERROR RenameRecording(const PVR_RECORDING& recording)
{
    return PVR_ERROR_NOT_IMPLEMENTED;
}

PVR_ERROR SetRecordingPlayCount(const PVR_RECORDING& recording, int count)
{
    return PVR_ERROR_NOT_IMPLEMENTED;
}

PVR_ERROR SetRecordingLastPlayedPosition(const PVR_RECORDING& recording, int lastplayedposition)
{
    return PVR_ERROR_NOT_IMPLEMENTED;
}

int GetRecordingLastPlayedPosition(const PVR_RECORDING& recording)
{
    return -1;
}

int GetTimersAmount()
{
    return -1;
}

PVR_ERROR GetTimers(ADDON_HANDLE handle)
{
    return PVR_ERROR_NOT_IMPLEMENTED;
}

PVR_ERROR AddTimer(const PVR_TIMER& timer)
{
    return PVR_ERROR_NOT_IMPLEMENTED;
}

PVR_ERROR DeleteTimer(const PVR_TIMER& timer, bool bForceDelete)
{
    return PVR_ERROR_NOT_IMPLEMENTED;
}

PVR_ERROR UpdateTimer(const PVR_TIMER& timer)
{
    return PVR_ERROR_NOT_IMPLEMENTED;
}

int GetCurrentClientChannel()
{
    return -1;
}

PVR_ERROR SignalStatus(PVR_SIGNAL_STATUS& signalStatus)
{
    return PVR_ERROR_NOT_IMPLEMENTED;
}

PVR_ERROR GetStreamProperties(PVR_STREAM_PROPERTIES* pProperties)
{
    return PVR_ERROR_NOT_IMPLEMENTED;
}

bool OpenRecordedStream(const PVR_RECORDING& recording)
{
    return false;
}

void CloseRecordedStream()
{
}

int ReadRecordedStream(unsigned char* pBuffer, unsigned int iBufferSize)
{
    return -1;
}

long long SeekRecordedStream(long long iPosition, int iWhence)
{
    return -1;
}

long long PositionRecordedStream()
{
    return -1;
}

long long LengthRecordedStream()
{
    return -1;
}

void DemuxReset()
{
}

void DemuxAbort()
{
}

void DemuxFlush()
{
}

DemuxPacket* DemuxRead()
{
    return NULL;
}

unsigned int GetChannelSwitchDelay()
{
    return 0;
}

void PauseStream(bool bPaused)
{
}

bool SeekTime(int time, bool backwards, double *startpts)
{
    return false;
}

void SetSpeed(int speed)
{
}

PVR_ERROR GetRecordingEdl(const PVR_RECORDING&, PVR_EDL_ENTRY edl[], int *size)
{
    return PVR_ERROR_NOT_IMPLEMENTED;
}

time_t GetPlayingTime() { return 0; }
time_t GetBufferTimeStart() { return 0; }
time_t GetBufferTimeEnd() { return 0; }
