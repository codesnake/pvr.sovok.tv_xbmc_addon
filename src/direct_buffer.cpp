/*
 *  Copyright (C) 2013 Alex Deryskyba (alex@codesnake.com)
 *  https://bitbucket.org/codesnake/pvr.sovok.tv_xbmc_addon
 *
 *  This file is part of pvr.sovok.tv XBMC Addon.
 *
 *  Foobar is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with pvr.sovok.tv XBMC Addon. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <libXBMC_addon.h>
#include "direct_buffer.h"

using namespace PLATFORM;

DirectBuffer::DirectBuffer(ADDON::CHelper_libXBMC_addon *addonHelper, const std::string &streamUrl) :
    m_addonHelper(addonHelper)
{
    m_streamHandle = m_addonHelper->OpenFile(streamUrl.c_str(), 0);
    if (!m_streamHandle)
        throw InputBufferException();
}

DirectBuffer::~DirectBuffer()
{
    m_addonHelper->CloseFile(m_streamHandle);
}

long long DirectBuffer::GetLength() const
{
    return -1;
}

long long DirectBuffer::GetPosition() const
{
    return -1;
}

int DirectBuffer::Read(unsigned char *buffer, unsigned int bufferSize)
{
    CLockObject lock(m_mutex);

    return m_addonHelper->ReadFile(m_streamHandle, buffer, bufferSize);
}

long long DirectBuffer::Seek(long long iPosition, int iWhence) const
{
    return -1;
}

bool DirectBuffer::SwitchStream(const std::string &newUrl)
{
    CLockObject lock(m_mutex);

    m_addonHelper->CloseFile(m_streamHandle);
    m_streamHandle = m_addonHelper->OpenFile(newUrl.c_str(), 0);

    return m_streamHandle != NULL;
}
