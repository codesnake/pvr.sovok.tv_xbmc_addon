#pragma once
/*
 *  Copyright (C) 2013 Alex Deryskyba (alex@codesnake.com)
 *  https://bitbucket.org/codesnake/pvr.sovok.tv_xbmc_addon
 *
 *  This file is part of pvr.sovok.tv XBMC Addon.
 *
 *  Foobar is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with pvr.sovok.tv XBMC Addon. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <libXBMC_pvr.h>
#include <string>
#include <map>
#include <set>
#include <vector>

struct SovokChannel
{
    int Id;
    std::string Name;
    std::string IconPath;
    bool IsRadio;

    bool operator <(const SovokChannel &anotherChannel) const
    {
        return Id < anotherChannel.Id;
    }
};

struct SovokGroup
{
    std::set<int> Channels;
};

struct SovokEpgEntry
{
    int ChannelId;
    std::string Title;
    std::string Description;
    time_t StartTime;
    time_t EndTime;
};

typedef std::map<int, SovokChannel> ChannelList;
typedef std::map<std::string, SovokGroup> GroupList;
typedef std::vector<SovokEpgEntry> EpgEntryList;
typedef std::map<std::string, std::string> ParamList;
typedef std::set<int> FavoriteList;

class AuthFailedException : public std::exception
{
};

class SovokTV
{
public:
    SovokTV(ADDON::CHelper_libXBMC_addon *addonHelper, const std::string &login, const std::string &password);
    ~SovokTV();

    const ChannelList &GetChannelList();
    EpgEntryList GetEpg(int channelId, time_t day);
    EpgEntryList GetEpg(int channelId, time_t startTime, time_t endTime);
    EpgEntryList GetEpgForAllChannels(time_t startTime, time_t endTime);
    const GroupList &GetGroupList();
    std::string GetUrl(int channelId);
    FavoriteList GetFavorites();

    int GetSreamerId() const { return m_streamerId; }
    void SetStreamerId(int streamerId);

private:
    EpgEntryList GetEpgForAllChannelsForNHours(time_t startTime, short numberOfHours);
    bool Login(const std::string &login, const std::string &password);
    void Logout();
    std::string CallApiFunction(const std::string &name, const ParamList &params = ParamList());
    std::string SendHttpRequest(const std::string &url, const ParamList &cookie = ParamList()) const;
    static size_t CurlWriteData(void *buffer, size_t size, size_t nmemb, void *userp);
    void BuildChannelAndGroupList();
    void LoadSettings();

    ADDON::CHelper_libXBMC_addon *m_addonHelper;
    std::string m_login;
    std::string m_password;
    ParamList m_sessionCookie;
    ChannelList m_channelList;
    GroupList m_groupList;
    EpgEntryList m_epgEntries;
    time_t m_lastEpgRequestStartTime;
    time_t m_lastEpgRequestEndTime;
    int m_streamerId;
};
